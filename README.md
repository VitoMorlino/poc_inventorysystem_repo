#### Showcase Video - Click to open in YouTube ####
[![SHOWCASE VIDEO - InventorySystem](https://image.ibb.co/cwAtUS/Video_Image_Inventory_System.png)](https://youtu.be/_k_BA0v6PH4 "SHOWCASE VIDEO - InventorySystem")

# README
#### To work on the project:
1. Clone the Development branch (not master)
2. Generate missing files after cloning by right-clicking the .uproject file and click Generate Visual Studio project files.
