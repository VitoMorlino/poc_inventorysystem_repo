// Copyright 2018 All Rights Reserved - Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseInteractable.generated.h"

UCLASS()
class INVENTORYSYSTEM_API ABaseInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseInteractable();

	UFUNCTION(BlueprintNativeEvent)
	void Interact(APlayerController* Controller);
	virtual void Interact_Implementation(APlayerController* Controller);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly)
	FString Name = "Name not set";

	UPROPERTY(EditDefaultsOnly)
	FString ActionVerb = "Interact";

	UFUNCTION(BlueprintCallable)
	FString GetInteractText() const;
	
	
};
