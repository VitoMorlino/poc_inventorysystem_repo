// Copyright 2018 All Rights Reserved - Vito Morlino

#include "InventoryManager.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Character.h"
#include "Components/CapsuleComponent.h"
#include "InventorySystemGameState.h"
#include "InventorySystemPlayerController.h"
#include "BasePickup.h"

#include "InventorySystem.h"


// Sets default values
AInventoryManager::AInventoryManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	InventorySlotLimit = 50;
	InventoryWeightLimit = 500;
	MaxStackSize = 4;
}

// Called when the game starts or when spawned
void AInventoryManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInventoryManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInventoryManager::SetMyController(AInventorySystemPlayerController * NewController)
{
	MyController = NewController;
}

bool AInventoryManager::GetItemInfoAtIndex(int32 Index, FInventoryItem& OutItemInfo)
{
	// Verify index in inventory
	if (!Inventory.IsValidIndex(Index))
	{
		LOGMSG_M(LogInventorySystem, Error, "Index [%d] is invalid", Index);

		return false;
	}

	// Set the OutItemInfo to the item in the slot at Index
	OutItemInfo = Inventory[Index].Item;

	return true;
}

bool AInventoryManager::FindFreeStack(FInventoryItem Item, int32& OutFoundIndex, int32& OutDistFromMaxStack)
{
	// Loop through inventory items, starting from the beginning/front/top
	for (int32 Index = 0; Index < Inventory.Num(); Index++)
	{
		// If the item is not the one we're looking for, skip to the next item
		if (Inventory[Index].Item != Item)
		{
			continue;
		}

		// Check if the item quantity is less than the MaxStackSize
		if (Inventory[Index].StackSize < MaxStackSize)
		{
			OutFoundIndex = Index;
			OutDistFromMaxStack = MaxStackSize - Inventory[OutFoundIndex].StackSize;

			return true;
		}
	}

	return false;
}

bool AInventoryManager::CombineStacksByIndex(int32 FromIndexA, int32 ToIndexB)
{
	// Verify indices in Inventory
	if (!Inventory.IsValidIndex(FromIndexA))
	{
		if (!Inventory.IsValidIndex(ToIndexB))
		{
			LOGMSG_M(LogInventorySystem, Error, "FromIndexA [%d] and ToIndexB [%d] are both invalid",
				FromIndexA, ToIndexB);
		}
		else
		{
			LOGMSG_M(LogInventorySystem, Error, "FromIndexA [%d] is invalid", FromIndexA);
		}

		return false;
	}
	else if (!Inventory.IsValidIndex(ToIndexB))
	{
		LOGMSG_M(LogInventorySystem, Error, "ToIndexB [%d] is invalid", ToIndexB);

		return false;
	}

	// Verify the indices aren't the same
	if (FromIndexA == ToIndexB)
	{
		LOGMSG_M(LogInventorySystem, Error, "FromIndexA [%d] and ToIndexB [%d] are the same index", FromIndexA, ToIndexB);

		return false;
	}

	// Verify the items match
	if (Inventory[FromIndexA].Item != Inventory[ToIndexB].Item)
	{
		LOGMSG_M(LogInventorySystem, Error, "Items don't match - FromIndexA's Item [%s] does not match ToIndexB's Item [%s]",
			*Inventory[FromIndexA].Item.ItemName.ToString(), *Inventory[ToIndexB].Item.ItemName.ToString());

		return false;
	}

	// Verify item is stackable - Note: Only one of the items needs to be checked because we know they match
	if (!Inventory[FromIndexA].Item.bCanBeStacked)
	{
		LOGMSG_M(LogInventorySystem, Error, "Item [%s] cannot be stacked", *Inventory[FromIndexA].Item.ItemName.ToString());

		return false;
	}

	// Verify ToIndexB's StackSize is not already at the MaxStackSize
	if (Inventory[ToIndexB].StackSize >= MaxStackSize)
	{
		LOGMSG_M(LogInventorySystem, Error, "ToIndexB's StackSize [%d] cannot be added to - MaxStackSize = %d",
			Inventory[ToIndexB].StackSize, MaxStackSize);

		return false;
	}

	/* If the program made it here, it is safe to start combining the stacks */

	int32 DistFromMaxStackSize = MaxStackSize - Inventory[ToIndexB].StackSize;

	// Check if we can add all of FromIndexA's stack to ToIndexB's stack
	if (DistFromMaxStackSize >= Inventory[FromIndexA].StackSize)
	{
		// Add FromIndexA's stacksize to ToIndexB's stacksize
		Inventory[ToIndexB].StackSize += Inventory[FromIndexA].StackSize;

		// Remove the inventory slot at FromIndexA
		Inventory.RemoveAt(FromIndexA);
	}
	else
	{
		// Load up ToIndexB's stack size to MaxStackSize
		Inventory[ToIndexB].StackSize = MaxStackSize;

		// Subtract ToIndexB's original DistFromMaxStackSize from FromIndexA's stacksize
		Inventory[FromIndexA].StackSize -= DistFromMaxStackSize;
	}

	// Reload inventory to reflect the change we just made
	AInventorySystemPlayerController* PlayerCon = Cast<AInventorySystemPlayerController>(MyController);
	if (PlayerCon) PlayerCon->ReloadInventory();

	return true;
}

bool AInventoryManager::SwapSlotsByIndex(int32 IndexA, int32 IndexB)
{
	// Verify indices in Inventory
	if (!Inventory.IsValidIndex(IndexA))
	{
		if (!Inventory.IsValidIndex(IndexB))
		{
			LOGMSG_M(LogInventorySystem, Error, "IndexA [%d] and IndexB [%d] are both invalid",
				IndexA, IndexB);
		}
		else
		{
			LOGMSG_M(LogInventorySystem, Error, "IndexA [%d] is invalid", IndexA);
		}

		return false;
	}
	else if (!Inventory.IsValidIndex(IndexB))
	{
		LOGMSG_M(LogInventorySystem, Error, "IndexB [%d] is invalid", IndexB);

		return false;
	}

	// Swap the slots
	Inventory.Swap(IndexA, IndexB);

	// Reload inventory to reflect the change we just made
	AInventorySystemPlayerController* PlayerCon = Cast<AInventorySystemPlayerController>(MyController);
	if (PlayerCon) PlayerCon->ReloadInventory();

	return true;
}

bool AInventoryManager::DragDropSlotOnSlot(int32 DraggedSlotIndex, int32 SlotDroppedOnIndex)
{
	// Verify indices in Inventory
	if (!Inventory.IsValidIndex(DraggedSlotIndex))
	{
		if (!Inventory.IsValidIndex(SlotDroppedOnIndex))
		{
			LOGMSG_M(LogInventorySystem, Error, "DraggedSlotIndex [%d] and SlotDroppedOnIndex [%d] are both invalid",
				DraggedSlotIndex, SlotDroppedOnIndex);
		}
		else
		{
			LOGMSG_M(LogInventorySystem, Error, "DraggedSlotIndex [%d] is invalid", DraggedSlotIndex);
		}

		return false;
	}
	else if (!Inventory.IsValidIndex(SlotDroppedOnIndex))
	{
		LOGMSG_M(LogInventorySystem, Error, "SlotDroppedOnIndex [%d] is invalid", SlotDroppedOnIndex);

		return false;
	}

	// Verify the indices aren't the same
	if (DraggedSlotIndex == SlotDroppedOnIndex)
	{
		LOGMSG_M(LogInventorySystem, Error, "DraggedSlotIndex [%d] and SlotDroppedOnIndex [%d] are the same index", DraggedSlotIndex, SlotDroppedOnIndex);

		return false;
	}

	// If the items in the slots match, are stackable, and neither slot is at the MaxStackSize, combine stacks
	if (Inventory[DraggedSlotIndex].Item == Inventory[SlotDroppedOnIndex].Item &&
		Inventory[DraggedSlotIndex].Item.bCanBeStacked &&
		Inventory[DraggedSlotIndex].StackSize != MaxStackSize &&
		Inventory[SlotDroppedOnIndex].StackSize != MaxStackSize)
	{
		return CombineStacksByIndex(DraggedSlotIndex, SlotDroppedOnIndex);
	}
	else
	{
		return SwapSlotsByIndex(DraggedSlotIndex, SlotDroppedOnIndex);
	}

}

bool AInventoryManager::SplitStackByIndex(int32 Index, int32 QuantityToSplit)
{
	// Verify index in Inventory
	if (!Inventory.IsValidIndex(Index))
	{
		LOGMSG_M(LogInventorySystem, Error, "Index [%d] is invalid", Index);

		return false;
	}

	// Verify stack size is greater than 1
	if (Inventory[Index].StackSize <= 1)
	{
		LOGMSG_M(LogInventorySystem, Warning, "StackSize must be greater than 1 to split stack", Index);

		return false;
	}

	// If the QuantityToSplit is -1, default the QuantityToSplit to half of the stack size
	if (QuantityToSplit == -1)
	{
		QuantityToSplit = Inventory[Index].StackSize / 2;
	}

	// Check if the stack size is less than or equal to the QuantityToSplit
	if (Inventory[Index].StackSize <= QuantityToSplit)
	{
		LOGMSG_M(LogInventorySystem, Warning, "StackSize [%d] must be greater than QuantityToSplit [%d]", Inventory[Index].StackSize, QuantityToSplit);

		return false;
	}

	// Verify there is enough space in the Inventory to add a new stack
	if (Inventory.Num() >= InventorySlotLimit)
	{
		LOGMSG(LogInventorySystem, Warning, "Not enough space in Inventory for a new stack");

		return false;
	}

	/* If the program made it here, it is safe to split the stack */

	// Remove the QuantityToSplit from the stack we're splitting
	Inventory[Index].StackSize -= QuantityToSplit;

	// Create a temporary new slot with the Item and QuantityToSplit
	FInventorySlot NewSlot(Inventory[Index].Item, QuantityToSplit);

	// Add the NewSlot to the Inventory
	Inventory.Add(NewSlot);

	// Reload inventory to reflect the changes we just made
	AInventorySystemPlayerController* PlayerCon = Cast<AInventorySystemPlayerController>(MyController);
	if (PlayerCon) PlayerCon->ReloadInventory();

	return true;
}

int32 AInventoryManager::GetInventoryWeight()
{
	int32 TotalWeight = 0;

	// Loop through each slot in the Inventory
	for (auto& Slot : Inventory)
	{
		// Add each item's weight to the TotalWeight
		TotalWeight += Slot.Item.Weight * Slot.StackSize;
	}

	return TotalWeight;
}

bool AInventoryManager::AddItemToInventoryByID(
	FName ID, 
	int32 QuantityToAdd, 
	bool ShouldLookForFreeStack, 
	bool IsRecursiveCall, 
	int32& OutTotalNumAdded)
{
	// Get the current Game State
	AInventorySystemGameState* GameState = Cast<AInventorySystemGameState>(GetWorld()->GetGameState());
	if (!GameState)
	{
		LOGMSG(LogInventorySystem, Error, "Game State not found");

		return false;
	}

	// Ask the GameState find the item with the matching ID
	FInventoryItem ItemToAdd;
	if (!GameState->GetItemInfo(ID, ItemToAdd))
	{
		LOGMSG_M(LogInventorySystem, Error, "Unable to find info for ID [%s]",
			*ID.ToString());

		return false;
	}

	// Verify QuantityToAdd is greater than zero
	if (QuantityToAdd <= 0)
	{
		LOGMSG(LogInventorySystem, Warning, "QuantityToAdd was not greater than zero");

		return false;
	}

	/** 
	*	@TODO - Modify weight limit check to account for multiple items (QuantityToAdd)
	*	reduce the QuantityToAdd if necessary and record the number of items not added (so they can stay on the ground for example) 
	*/
	// Check if inventory weight limit is reached
	int32 CurrentWeight = GetInventoryWeight();
	if (CurrentWeight + ItemToAdd.Weight > InventoryWeightLimit)
	{
		LOGMSG(LogInventorySystem, Warning, "Weight limit reached - Cannot add item to inventory");
		LOGMSG_M(LogInventorySystem, Warning, "Item Weight: [%d] --- Current Weight: [%d] / Weight Limit: [%d]",
			ItemToAdd.Weight, CurrentWeight, InventoryWeightLimit);

		return false;
	}

	// Check if this was the first call to this function (not a recursive call)
	if (!IsRecursiveCall)
	{
		// Initialize OutTotalNumAdded to 0 because this is not a recursive call (it's the first call)
		OutTotalNumAdded = 0;
	}

	// Create a temporary new slot with the ItemToAdd
	FInventorySlot NewSlot(ItemToAdd);

	// Number of items to continue trying to add when this function completes
	int32 RemainderToAdd = 0;

	// If the ItemToAdd can be stacked
	if (ItemToAdd.bCanBeStacked)
	{
		// Find a free stack to add the item to
		if (ShouldLookForFreeStack)
		{
			int32 FoundIndex = INDEX_NONE;
			int32 DistanceFromMaxStack = -1;

			// If a free stack is found
			if (FindFreeStack(ItemToAdd, FoundIndex, DistanceFromMaxStack))
			{
				int32 Remainder = QuantityToAdd - DistanceFromMaxStack;

				if (Remainder <= 0)
				{
					// Add the QuantityToAdd to the existing stack
					Inventory[FoundIndex].StackSize += QuantityToAdd;

					// Increment OutTotalNumAdded by the amount we added to the stack
					OutTotalNumAdded += QuantityToAdd;

					/* No further action is required because we're not adding a new slot, so reload inventory and return true */

					// Reload inventory to reflect the change we just made
					AInventorySystemPlayerController* PlayerCon = Cast<AInventorySystemPlayerController>(MyController);
					if (PlayerCon) PlayerCon->ReloadInventory();

					return true;
				}
				else
				{
					// Set the stack size of the found item to MaxStackSize
					Inventory[FoundIndex].StackSize = MaxStackSize;

					// Increment OutTotalNumAdded by the amount we added to the stack
					OutTotalNumAdded += DistanceFromMaxStack;

					//Recursively call this function again with the Remainder
					return AddItemToInventoryByID(ID, Remainder, ShouldLookForFreeStack, true, OutTotalNumAdded);
				}
			}
		}

		// Check if the QuantityToAdd is more than the MaxStackSize
		int32 Remainder = QuantityToAdd - MaxStackSize;
		if (Remainder > 0)
		{
			// Set the new slot's stack size to the MaxStackSize
			NewSlot.StackSize = MaxStackSize;

			// Set the RemainderToAdd to our local Remainder
			RemainderToAdd = Remainder;
		}
		else
		{
			// Set the new slot's stack size to the QuantityToAdd
			NewSlot.StackSize = QuantityToAdd;
		}
	} 
	else if (!ItemToAdd.bCanBeStacked) // Handles adding multiple non-stackable items
	{
		// Set the RemainderToAdd to QuantityToAdd - 1
		RemainderToAdd = QuantityToAdd - 1;
	}

	// If inventory slot limit is reached
	if (Inventory.Num() >= InventorySlotLimit)
	{
		LOGMSG(LogInventorySystem, Warning, "Slot limit reached - Cannot add item to inventory");

		return false;
	}

	// Add the item to our inventory
	Inventory.Add(NewSlot);

	// Increment OutTotalNumAdded by the quantity we gave to the new stack
	OutTotalNumAdded += NewSlot.StackSize;
	
	// If there are still more items left to add
	if (RemainderToAdd > 0)
	{
		// Recursively call this function again with the RemainderToAdd
		return AddItemToInventoryByID(ID, RemainderToAdd, ShouldLookForFreeStack, true, OutTotalNumAdded);
	}

	// Reload inventory to reflect the change we just made
	AInventorySystemPlayerController* PlayerCon = Cast<AInventorySystemPlayerController>(MyController);
	if (PlayerCon) PlayerCon->ReloadInventory();

	LOGMSG_M(LogInventorySystem, Log, "Item added to inventory: [%s] x[%d]", *ItemToAdd.ItemName.ToString(), OutTotalNumAdded);

	// Tell Blueprint about what we just obtained to handle further action there
	if (PlayerCon) PlayerCon->OnObtainedItem(ItemToAdd, OutTotalNumAdded);

	return true;
}

bool AInventoryManager::RemoveItemFromInventoryByID(FName ID, int32 QuantityToRemove)
{
	// Find the last inventory slot with a matching ID
	int32 FoundIndex = INDEX_NONE;
	bool bHasItem = Inventory.FindLast(FInventorySlot(FInventoryItem(ID)), FoundIndex);

	if (FoundIndex == INDEX_NONE)
	{
		LOGMSG_M(LogInventorySystem, Warning, "Could not find ID [%s] in Inventory",
			*ID.ToString());

		return false;
	}

	return RemoveItemFromInventoryByIndex(FoundIndex, QuantityToRemove, true);
}

bool AInventoryManager::RemoveItemFromInventoryByIndex(int32 Index, int32 QuantityToRemove, bool RemoveFromOtherStacks)
{
	// Verify index in Inventory
	if (!Inventory.IsValidIndex(Index))
	{
		LOGMSG_M(LogInventorySystem, Error, "Index [%d] is invalid", Index);

		return false;
	}

	if (QuantityToRemove < Inventory[Index].StackSize)
	{
		Inventory[Index].StackSize -= QuantityToRemove;
	}
	else
	{
		// Save the Remainder before removing the item
		int32 Remainder = QuantityToRemove - Inventory[Index].StackSize;
		// Save the ItemID before removing the item
		FName RemovedItemID = Inventory[Index].Item.ItemID;

		// Remove the inventory slot
		Inventory.RemoveAt(Index);

		if (Remainder > 0 && RemoveFromOtherStacks)
		{
			/** Reload the Inventory to reflect the change we just made
			*	@note This is in case we're trying to remove more than the player had and RemoveItemFromInventoryByID will return false
			*/
			AInventorySystemPlayerController* PlayerCon = Cast<AInventorySystemPlayerController>(MyController);
			if (PlayerCon) PlayerCon->ReloadInventory();

			return RemoveItemFromInventoryByID(RemovedItemID, Remainder);
		}
	}

	// Reload inventory to reflect the change we just made
	AInventorySystemPlayerController* PlayerCon = Cast<AInventorySystemPlayerController>(MyController);
	if (PlayerCon) PlayerCon->ReloadInventory();

	return true;
}

bool AInventoryManager::DropItemFromInventoryByIndex(int32 Index, int32 Quantity)
{
	// Verify index in Inventory
	if (!Inventory.IsValidIndex(Index))
	{
		LOGMSG_M(LogInventorySystem, Error, "Index [%d] is invalid", Index);

		return false;
	}

	// Verify item can be dropped
	if (!Inventory[Index].Item.CanBeDropped())
	{
		LOGMSG_M(LogInventorySystem, Warning, "[%s] cannot be dropped.",
			*Inventory[Index].Item.ItemName.ToString());

		return false;
	}

	// Verify ItemPickup is set in the ItemDB entry
	if (Inventory[Index].Item.ItemPickup == nullptr)
	{
		LOGMSG_M(LogInventorySystem, Error, "ItemPickup not set for [%s]",
			*Inventory[Index].Item.ItemName.ToString());

		return false;
	}

	// Verify access the world object
	if (!GetWorld())
	{
		LOGMSG(LogInventorySystem, Error, "World Object could not be found.");

		return false;
	}

	// Verify access to the player controller
	AInventorySystemPlayerController* PlayerCon = Cast<AInventorySystemPlayerController>(MyController);
	if (!PlayerCon)
	{
		LOGMSG(LogInventorySystem, Error, "Player Controller could not be found.");

		return false;
	}

	/* If the program made it here, it is safe to drop the item */

	// Get the transform of the player character
	FTransform SpawnTransform = PlayerCon->GetCharacter()->GetActorTransform();
	int32 ZMod = -PlayerCon->GetCharacter()->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	SpawnTransform.SetLocation(SpawnTransform.GetLocation() + FVector(550, 0, ZMod));

	// Spawn the item in the game world at the location/rotation of the player character
	AActor* SpawnedActor = GetWorld()->SpawnActor(Inventory[Index].Item.ItemPickup, &SpawnTransform);

	// Verify the spawn was successful
	if (!SpawnedActor)
	{
		LOGMSG(LogInventorySystem, Warning, "Spawn Failed - Canceling Item Drop");

		return false;
	}

	// Remove the dropped item from Inventory
	if (Inventory[Index].StackSize > Quantity)
	{
		// Reduce stack size
		Inventory[Index].StackSize -= Quantity;
	}
	else if (Inventory[Index].StackSize == Quantity)
	{
		// Remove the item
		Inventory.RemoveAt(Index);
	}
	else if (Inventory[Index].StackSize < Quantity)
	{
		int32 Remainder = Quantity - Inventory[Index].StackSize;
		FName ItemID = Inventory[Index].Item.ItemID;

		// Remove the item
		Inventory.RemoveAt(Index);

		// Remove the remainder from Inventory by ID
		return RemoveItemFromInventoryByID(ItemID, Remainder);
	}

	// Reload inventory to reflect the change we just made
	if (PlayerCon) PlayerCon->ReloadInventory();

	return true;
}

