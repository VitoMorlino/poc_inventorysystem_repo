// Copyright 2018 All Rights Reserved - Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "InventorySystem.h"
#include "GameFramework/PlayerController.h"
#include "InventorySystemGameMode.h"
#include "BaseInteractable.h"
#include "InventoryManager.h"
#include "InventorySystemPlayerController.generated.h"


UCLASS()
class AInventorySystemPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AInventorySystemPlayerController();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void ReloadInventory();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void OnObtainedItem(FInventoryItem ObtainedItem, int32 ObtainedAmount);

	/** The player's currently targeted Interactable */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class ABaseInteractable* CurrentInteractable;

	/** The player's inventory */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	AInventoryManager* InventoryManager;

protected:
	virtual void BeginPlay() override;

	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();

	/** Navigate player to the current touch location. */
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);
	
	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();

	void Interact();

};


