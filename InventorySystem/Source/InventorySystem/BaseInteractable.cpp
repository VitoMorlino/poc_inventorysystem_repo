// Copyright 2018 All Rights Reserved - Vito Morlino

#include "BaseInteractable.h"


// Sets default values
ABaseInteractable::ABaseInteractable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ABaseInteractable::Interact_Implementation(APlayerController * Controller)
{
	return;
}

// Called when the game starts or when spawned
void ABaseInteractable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseInteractable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FString ABaseInteractable::GetInteractText() const
{
	return FString::Printf(TEXT("%s : Press E to %s"), *Name, *ActionVerb);
}

