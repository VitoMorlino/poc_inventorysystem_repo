// Copyright 2018 All Rights Reserved - Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "BaseInteractable.h"
#include "BasePickup.generated.h"

/**
 * 
 */
UCLASS()
class INVENTORYSYSTEM_API ABasePickup : public ABaseInteractable
{
	GENERATED_BODY()

public:

	ABasePickup();

	UFUNCTION(BlueprintNativeEvent)
	void Collect(APlayerController* Controller);
	virtual void Collect_Implementation(APlayerController* Controller);

	UFUNCTION(BlueprintCallable)
	bool UseItem(APlayerController* Controller);

	UFUNCTION(BlueprintImplementableEvent)
	void OnUsed(APlayerController* Controller);

	FName GetItemID();

protected:

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* PickupMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FName ItemID;
	
};


