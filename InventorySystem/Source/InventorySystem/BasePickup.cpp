// Copyright 2018 All Rights Reserved - Vito Morlino

#include "BasePickup.h"
#include "InventorySystem.h"
#include "InventorySystemGameState.h"
#include "InventorySystemPlayerController.h"
#include "InventoryManager.h"

ABasePickup::ABasePickup()
{
	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>("PickupMesh");
	RootComponent = Cast<USceneComponent>(PickupMesh);

	ItemID = "Please enter an ID for the item";
	ActionVerb = "Pick Up";
}

void ABasePickup::Collect_Implementation(APlayerController* Controller)
{
	int32 NumCollected = 0;

	AInventorySystemPlayerController* IController = Cast<AInventorySystemPlayerController>(Controller);
	if (IController->InventoryManager->AddItemToInventoryByID(ItemID, 1, true, false, NumCollected))
	{
		Destroy();
	}
}

bool ABasePickup::UseItem(APlayerController* Controller)
{
	// Get the current Game State
	AInventorySystemGameState* GameState = Cast<AInventorySystemGameState>(GetWorld()->GetGameState());
	if (!GameState)
	{
		LOGMSG(LogInventorySystem, Error, "Game State not found - Cannot use item");

		return false;
	}

	// Get the Item Database
	UDataTable* ItemTable = GameState->GetItemDB();
	if (!ItemTable)
	{
		LOGMSG(LogInventorySystem, Error, "Item Database not found - Cannot use item");

		return false;
	}

	// Check Item Database to find the item with the matching ID
	FInventoryItem* ItemToUse = ItemTable->FindRow<FInventoryItem>(ItemID, "");
	if (!ItemToUse)
	{
		LOGMSG_M(LogInventorySystem, Error, "Unable to find ID [%s] in item database - Cannot use item",
			*ItemID.ToString());

		return false;
	}

	// Verify that the item can be used
	if (!ItemToUse->bCanBeUsed)
	{
		LOGMSG_M(LogInventorySystem, Warning, "[%s] cannot be used - bCanBeUsed is false",
			*ItemToUse->ItemName.ToString());

		return false;
	}

	LOGMSG_M(LogTemp, Warning, "Using Item [%s] (C++ Call)", *ItemToUse->ItemName.ToString())

	//Call the BP implementable event OnUsed for specific item functionality to be handled in BP
	OnUsed(Controller);

	return true;
}

FName ABasePickup::GetItemID()
{
	return ItemID;
}
