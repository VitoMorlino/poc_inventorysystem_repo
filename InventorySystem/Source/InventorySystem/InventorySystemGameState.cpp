// Copyright 2018 All Rights Reserved - Vito Morlino

#include "InventorySystemGameState.h"
#include "InventorySystem.h"
#include "Engine/DataTable.h"

AInventorySystemGameState::AInventorySystemGameState()
{

}

bool AInventorySystemGameState::GetItemInfo(FName ID, FInventoryItem& OutItemInfo)
{
	// Verify the Item Database is set
	if (!ItemDB)
	{
		LOGMSG(LogInventorySystem, Error, "Item Database not found");

		return false;
	}

	// Check Item Database to find the item with the matching ID
	FInventoryItem* Item = ItemDB->FindRow<FInventoryItem>(ID, "");
	if (!Item)
	{
		LOGMSG_M(LogInventorySystem, Error, "Unable to find ID [%s] in item database",
			*ID.ToString());

		return false;
	}

	OutItemInfo = *Item;

	return true;
}
