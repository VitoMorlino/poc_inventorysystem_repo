// Copyright 2018 All Rights Reserved - Vito Morlino
#include "InventoryItem.h"

//Constructor
FInventoryItem::FInventoryItem(FName ID)
{
	ItemID = ID;
	ItemName = FText::FromString("Please enter a name for the item");
	Description = FText::FromString("Please enter a description for the item");
	ActionVerb = FText::FromString("Use");

	ItemValue = 0;
	Weight = 1;

	ItemCategory = EItemCategories::None;

	bCanBeStacked = false;
	bCanBeUsed = false;
	bDestroyOnUse = true;
}

bool FInventoryItem::CanBeDropped()
{
	return ItemCategory != EItemCategories::QuestItem;
}
