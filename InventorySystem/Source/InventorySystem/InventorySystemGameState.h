// Copyright 2018 All Rights Reserved - Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "InventoryItem.h"
#include "InventorySystemGameState.generated.h"

/**
 * 
 */
UCLASS()
class INVENTORYSYSTEM_API AInventorySystemGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	AInventorySystemGameState();

	class UDataTable* GetItemDB() const { return ItemDB; }
	
	/**
	*	Gets the item info of the item with the passed ID
	*	@param ID - The ID of the item to get the info of
	*	@param OutItemInfo - Upon successful return, will contain the item info of the item with the passed ID
	*	@return Whether an item with the passed ID was found successfully
	*/
	UFUNCTION(BlueprintCallable)
	bool GetItemInfo(FName ID, struct FInventoryItem& OutItemInfo);

protected:

	UPROPERTY(EditDefaultsOnly)
		class UDataTable* ItemDB;
	
	
};
