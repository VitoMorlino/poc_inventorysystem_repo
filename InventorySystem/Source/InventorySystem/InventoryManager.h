// Copyright 2018 All Rights Reserved - Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InventoryItem.h"
#include "InventoryManager.generated.h"

USTRUCT(BlueprintType)
struct FInventorySlot
{
	GENERATED_BODY()

	FInventorySlot()
	{
		StackSize = 1;
	}

	FInventorySlot(FInventoryItem NewItem, int32 NewQuantity = 1)
	{
		Item = NewItem;
		StackSize = NewQuantity;
	}

	/* The item held in this slot */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FInventoryItem Item;

	/** The quantity of items held in this slot*/
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		int32 StackSize;

	bool operator==(const FInventorySlot& Slot) const
	{
		return Item.ItemID == Slot.Item.ItemID;
	}
};

UCLASS(Blueprintable, BlueprintType)
class INVENTORYSYSTEM_API AInventoryManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInventoryManager();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetMyController(class AInventorySystemPlayerController* NewController);

	//@TODO - Define this function - If you're capable of adding this functionality, you should be able to figure out what it should do based on variable names
	//bool VerifyInventoryContents(bool RemoveInvalidSlots, bool FixStackSizeError, TArray<FInventorySlot>& OutInvalidSlots);

	/**
	*	Finds a free stack of items that match the passed item
	*	@param Index - The index of the item to get info for
	*	@param OutItemInfo - Upon successful return, will contain the item info
	*	@return Whether the item info was found successfully
	*/
	UFUNCTION(BlueprintCallable, Category = "Utils")
		bool GetItemInfoAtIndex(int32 Index, FInventoryItem& OutItemInfo);

	/**
	*	Finds a free stack of items that match the passed item
	*	@param Item - The kind of item to look for a free stack of
	*	@param OutFoundIndex - Upon successful return, will contain the index of the found free stack
	*	@param OutDistFromMaxStack - Upon successul return, will contain the stack's distance from the MaxStackSize
	*	@return Whether a free stack was found successfully
	*/
	UFUNCTION(BlueprintCallable, Category = "Utils")
		bool FindFreeStack(FInventoryItem Item, int32& OutFoundIndex, int32& OutDistFromMaxStack);

	/**
	*	Combines the stack sizes of two inventory slots by taking from one and adding to the other
	*	@param FromIndexA - The stack being taken from
	*	@param ToIndexB - The stack being added to
	*	@return Whether the combination was successful
	*/
	UFUNCTION(BlueprintCallable, Category = "Utils")
		bool CombineStacksByIndex(int32 FromIndexA, int32 ToIndexB);

	/**
	*	Swaps the positions of two inventory slots
	*	@return Whether the slots were swapped successfully
	*/
	UFUNCTION(BlueprintCallable, Category = "Utils")
		bool SwapSlotsByIndex(int32 IndexA, int32 IndexB);

	/**
	*	Determines if it is appropriate to combine the stacks or swap the slots,
	*	then calls CombineStacksByIndex or SwapSlotsByIndex accordingly
	*	@param DraggedSlotIndex - The index of the dragged slot
	*	@param SlotDroppedOnIndex - The index of the slot that was dropped onto
	*	@return Whether the slots were combined or swapped successfully
	*/
	UFUNCTION(BlueprintCallable, Category = "Utils")
		bool DragDropSlotOnSlot(int32 DraggedSlotIndex, int32 SlotDroppedOnIndex);

	/**
	*	Splits a stack of items and adds the split items as a new stack
	*	@param Index - The index of the stack to split
	*	@param QuantityToSplit - The number of items to split (If QuantityToSplit = -1, split the stack in half - rounding down)
	*	@return Whether the stack was split successfully
	*/
	UFUNCTION(BlueprintCallable, Category = "Utils")
		bool SplitStackByIndex(int32 Index, int32 QuantityToSplit = -1);

	/**
	*	Adds the weight of all items in the inventory and returns the total
	*	@return The total weight of all items in the Inventory
	*/
	UFUNCTION(BlueprintCallable, Category = "Utils")
		int32 GetInventoryWeight();

	/**
	*	Adds an item to the Inventory
	*	@param ID - The ID of the item to add
	*	@param Quantity - The quantity to add
	*	@param ShouldLookForFreeStack - Whether to try and add the item to a free stack
	*	@param IsRecursiveCall - False if not calling this function from within itself
	*	@param OutTotalNumAdded - Upon successful return, will contain the total number of items added
	*	@return Whether the item was added successfully
	*/
	UFUNCTION(BlueprintCallable, Category = "Utils")
		bool AddItemToInventoryByID(
			FName ID,
			int32 Quantity,
			bool ShouldLookForFreeStack,
			bool IsRecursiveCall, /*bool IgnoreWeightLimit, bool IgnoreSlotLimit,*/ 
			int32& OutTotalNumAdded);

	/**
	*	Removes an item from the Inventory, deleting it
	*	@param ID - The ID of the item to remove
	*	@param Quantity - The quantity to remove
	*	@return Whether the item was removed successfully
	*/
	UFUNCTION(BlueprintCallable, Category = "Utils")
		bool RemoveItemFromInventoryByID(FName ID, int32 Quantity = 1);

	/**
	*	Removes an item from the Inventory, deleting it
	*	@param Index - The index of the item to remove
	*	@param Quantity - The quantity to remove
	*	@return Whether the item was removed successfully
	*/
	UFUNCTION(BlueprintCallable, Category = "Utils")
		bool RemoveItemFromInventoryByIndex(int32 Index, int32 Quantity = 1, bool RemoveFromOtherStacks = false);

	/**
	*	Removes an item from the Inventory and drops it into the game world at the player's location
	*	@param Index - The index of the item to drop
	*	@param Quantity - The quantity to drop
	*	@return Whether the item was dropped successfully
	*/
	UFUNCTION(BlueprintCallable, Category = "Utils")
		bool DropItemFromInventoryByIndex(int32 Index, int32 Quantity = 1);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	class AInventorySystemPlayerController* MyController;

	/** The inventory containing all held items */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FInventorySlot> Inventory;

	/** The amount of money the player is holding */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		int32 Money;

	/** The maximum number of slots available in the inventory */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 InventorySlotLimit;

	/** The maximum total weight limit of items held in the inventory */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 InventoryWeightLimit;

	/** The maximum stack size of stackable items in the inventory */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MaxStackSize;
	
};
