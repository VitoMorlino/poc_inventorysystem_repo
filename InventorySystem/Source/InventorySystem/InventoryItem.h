// Copyright 2018 All Rights Reserved - Vito Morlino

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/DataTable.h"
#include "Engine/Texture2D.h"
#include "InventoryItem.generated.h"

UENUM(BlueprintType)
enum class EItemCategories : uint8
{
	None,
	Consumable,
	Equipment,
	QuestItem,
};

USTRUCT(BlueprintType)
struct FInventoryItem : public FTableRowBase
{
	GENERATED_BODY()

public:
	FInventoryItem(FName ID = "Please enter an ID for the item");

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FName ItemID;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<class ABasePickup> ItemPickup;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FText ItemName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FText Description;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		EItemCategories ItemCategory;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FText ActionVerb;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		int32 ItemValue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		int32 Weight;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTexture2D* Thumbnail;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		bool bCanBeStacked;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		bool bCanBeUsed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		bool bDestroyOnUse;

	bool CanBeDropped();

	bool operator==(const FInventoryItem& Item) const
	{
		return (ItemID == Item.ItemID);
	}

	bool operator!=(const FInventoryItem& Item) const
	{
		return (ItemID != Item.ItemID);
	}

};
